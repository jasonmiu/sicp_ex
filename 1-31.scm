     ;; *Exercise 1.31:*
     ;;   a. The `sum' procedure is only the simplest of a vast number of
     ;;      similar abstractions that can be captured as higher-order
     ;;      procedures.(3)  Write an analogous procedure called `product'
     ;;      that returns the product of the values of a function at
     ;;      points over a given range.  Show how to define `factorial' in
     ;;      terms of `product'.  Also use `product' to compute
     ;;      approximations to [pi] using the formula(4)

     ;;           pi   2 * 4 * 4 * 6 * 6 * 8 ...
     ;;           -- = -------------------------
     ;;            4   3 * 3 * 5 * 5 * 7 * 7 ...

     ;;   b. If your `product' procedure generates a recursive process,
     ;;      write one that generates an iterative process.  If it
     ;;      generates an iterative process, write one that generates a
     ;;      recursive process.


(define (inc x)
  (+ x 1))

(define (double x)
  (* 2 x))

(define (even? x)
  (if (= (remainder x 2) 0)
      true
      false))

(define (product term-func a next-func b)
  (if (> a b)
      1
      (* (term-func a)
         (product term-func (next-func a) next-func b))))

(product double 1 inc 5) ; 3840

(define (product-iter term-func a next-func b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next-func a) (* (term-func a) result))))
  (iter a 1))


(product-iter double 1 inc 5) ; 3840


(define (up-term i)
  (cond ((= i 1) 2)
        ((and (> i 1) (even? i))
         (+ i 2))
        ((and (> i 1) (not (even? i)))
         (+ (- i 1) 2))))

(up-term 1)
(up-term 2)
(up-term 3)
(up-term 4)
(up-term 5)
(up-term 6)
(up-term 7)

(define (low-term i)
  (if (even? i)
      (+ (- i 1) 2)
      (+ i 2)))

(low-term 1)
(low-term 2)
(low-term 3)
(low-term 4)
(low-term 5)
(low-term 6)
(low-term 7)


(product up-term 1 inc 6) ; 9216
(product low-term 1 inc 6) ; 11025

(define (pi a b)
  (* (/ (product up-term a inc b)
        (product low-term a inc b))
     4.0))

(pi 1 6)
(pi 1 20000)
;; (pi 1 40000) ;Aborting!: maximum recursion depth exceeded


;; pi   2 * 4 * 4 * 6 * 6 * 8 ... <-- up-term
;; -- = -------------------------
;;  4   3 * 3 * 5 * 5 * 7 * 7 ... <-- low-term

(define (pi-iter a b)
  (* (/ (product-iter up-term a inc b)
        (product-iter low-term a inc b))
     4.0))

(pi-iter 1 6)
(pi-iter 1 20000)
(pi-iter 1 40000) ;; no recursion depth with iteration
  
