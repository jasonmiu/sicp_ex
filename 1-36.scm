     ;; *Exercise 1.36:* Modify `fixed-point' so that it prints the
     ;; sequence of approximations it generates, using the `newline' and
     ;; `display' primitives shown in *Note Exercise 1-22::.  Then find a
     ;; solution to x^x = 1000 by finding a fixed point of x |->
     ;; `log'(1000)/`log'(x).  (Use Scheme's primitive `log' procedure,
     ;; which computes natural logarithms.)  Compare the number of steps
     ;; this takes with and without average damping.  (Note that you
     ;; cannot start `fixed-point' with a guess of 1, as this would cause
     ;; division by `log'(1) = 0.)

(define tolerance 0.00001)

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (power-self y)
  (fixed-point (lambda (x) (/ (log y) (log x)))
               1.5))

;; fixed point of x^x = 1000
(power-self 1000)

(define (fixed-point-print f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (newline)
      (display guess)
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

  
(define (power-self y)
  (fixed-point-print (lambda (x) (/ (log y) (log x)))
                     1.5))

;; fixed point of x^x = 1000 with printing each guess
(power-self 1000)

(define (average a b)
  (/ (+ a b) 2))

(define (power-self y)
  (fixed-point-print (lambda (x) (average
                                  x
                                  (/ (log y) (log x))))
                     1.5))

;; fixed point of x^x = 1000 with avg damping and printing each guess
(power-self 1000)
