     ;; *Exercise 1.17:* The exponentiation algorithms in this section are
     ;; based on performing exponentiation by means of repeated
     ;; multiplication.  In a similar way, one can perform integer
     ;; multiplication by means of repeated addition.  The following
     ;; multiplication procedure (in which it is assumed that our language
     ;; can only add, not multiply) is analogous to the `expt' procedure:

     ;;      (define (* a b)
     ;;        (if (= b 0)
     ;;            0
     ;;            (+ a (* a (- b 1)))))

     ;; This algorithm takes a number of steps that is linear in `b'.  Now
     ;; suppose we include, together with addition, operations `double',
     ;; which doubles an integer, and `halve', which divides an (even)
     ;; integer by 2.  Using these, design a multiplication procedure
     ;; analogous to `fast-expt' that uses a logarithmic number of steps.


;; Think the loop invariant is : a * b + c = constant, c is the final anwser
;; we want. We want to transfer the value of a * b to c.
;; a * b = b + b + ... + b
;;         \_____ a ____/
;;
;; a * b = [b + b + ..... + b] + [b + b + ..... + b]
;;         \____ a/2 _______/    \____ a/2___ ____/
;;
;; a * b = [b + b + ..... + b] * 2
;;         \____ a/2 _______/
;;
;; a * b = [b + b + ... + b] * 2 * 2
;;         \____ a/2/2 ___/
;;
;; a * b = [b + b + . + b] * 2 * 2 * 2
;;         \__ a/2/2/2 _/
;;
;; a * b = b * 2 * 2 * ... *2
;;             \____ a/2 ___/
;;
;; So a * b is (b * 2) loop a/2 times. b is the accumuate variable in the loop.
;; To keep a * b + c = constant, c is adding up the residual of the b that
;; does not added up in the even loop. For example:
;; a  b  c
;; 5  5  0
;; 4  5  5
;; 2 10  5
;; 1 20  5  <-- a * b + c = constant
;; 0 20 25  <-- return

(define (double x)
  (+ x x))

(define (halve x)
  (/ x 2))

(define (even? x)
  (= (remainder x 2) 0))


(define (mul-iter a b c)
  (cond ((= a 0) c)
        (else
         (if (even? a)
             (mul-iter (halve a) (double b) c)
             (mul-iter (- a 1) b (+ b c))))))

(define (mul a b)
  (mul-iter a b 0))

(mul 1 2)
(* 1 2)
(mul 2 2)
(* 2 2)
(mul 2 4)
(* 2 4)
(mul 3 6)
(* 3 6)
(mul 5 5)
(* 5 5)
(mul 9 9)
(* 9 9)
(mul 1024 256)
(* 1024 256)
(mul 32721 4388685)
(* 32721 4388685)


;; This is the recursive version
(define (mul-re a b)
  (cond ((= a 0) 0)
        (else
         (if (even? a)
             (mul-re (halve a) (double b))
             (+ b (mul-re (- a 1) b))))))

(mul-re 1 2)
(* 1 2)
(mul-re 2 2)
(* 2 2)
(mul-re 2 4)
(* 2 4)
(mul-re 3 6)
(* 3 6)
(mul-re 5 5)
(* 5 5)
(mul-re 9 9)
(* 9 9)
(mul-re 1024 256)
(* 1024 256)
(mul-re 32721 4388685)
(* 32721 4388685)
