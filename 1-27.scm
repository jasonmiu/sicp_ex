(define (square x)
  (* x x))

(define (even? x)
  (= (remainder x 2) 0))

;; Fast prime test with Fermat test
(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else false)))

(fast-prime? 101 10)
(fast-prime? 100 10)
     ;; *Exercise 1.27:* Demonstrate that the Carmichael numbers listed in
     ;; *Note Footnote 1-47:: really do fool the Fermat test.  That is,
     ;; write a procedure that takes an integer n and tests whether a^n is
     ;; congruent to a modulo n for every a<n, and try your procedure on
     ;; the given Carmichael numbers.


;; A fermat test without using random number,
;; but user input 'x'
(define (fermat-test-fix n x)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it x))

;; A carmichael prime, will pass all fermat tests,
;; from modulo 1 to n-1. User a loop to test this range.
;; If in a loop that find a case that fail the fermat test,
;; number n is not a prime and return false at once.
;; If can finish all loop until the modulo is 1,
;; this n passed all fermat test. It can be a real prime,
;; or a carmichael prime.
(define (carmichael-prime? n)
  (define (check-all n cnt)
    (if (= cnt 1)
        #t
        (if (fermat-test-fix n cnt)
            (check-all n (- cnt 1))
            #f)))
  (check-all n (- n 1)))

(carmichael-prime? 561)
(carmichael-prime? 1105)
(carmichael-prime? 1729)
(carmichael-prime? 2465)
(carmichael-prime? 2821)
(carmichael-prime? 6601)
(carmichael-prime? 2)
(carmichael-prime? 101)
(carmichael-prime? 100)
(carmichael-prime? 65535)




