     ;; *Exercise 1.28:* One variant of the Fermat test that cannot be
     ;; fooled is called the "Miller-Rabin test" (Miller 1976; Rabin
     ;; 1980).  This starts from an alternate form of Fermat's Little
     ;; Theorem, which states that if n is a prime number and a is any
     ;; positive integer less than n, then a raised to the (n - 1)st power
     ;; is congruent to 1 modulo n.  To test the primality of a number n
     ;; by the Miller-Rabin test, we pick a random number a<n and raise a
     ;; to the (n - 1)st power modulo n using the `expmod' procedure.
     ;; However, whenever we perform the squaring step in `expmod', we
     ;; check to see if we have discovered a "nontrivial square root of 1
     ;; modulo n," that is, a number not equal to 1 or n - 1 whose square
     ;; is equal to 1 modulo n.  It is possible to prove that if such a
     ;; nontrivial square root of 1 exists, then n is not prime.  It is
     ;; also possible to prove that if n is an odd number that is not
     ;; prime, then, for at least half the numbers a<n, computing a^(n-1)
     ;; in this way will reveal a nontrivial square root of 1 modulo n.
     ;; (This is why the Miller-Rabin test cannot be fooled.)  Modify the
     ;; `expmod' procedure to signal if it discovers a nontrivial square
     ;; root of 1, and use this to implement the Miller-Rabin test with a
     ;; procedure analogous to `fermat-test'.  Check your procedure by
     ;; testing various known primes and non-primes.  Hint: One convenient
     ;; way to make `expmod' signal is to have it return 0.


(define (square x)
  (* x x))

(define (even? x)
  (= (remainder x 2) 0))

;; Check if x is the nontrivial square root of 1 mod n
;; ie. if (x^2) mod n == 1 mod n, x is nontrivial square root of 1 mod n
(define (nontrivial-sqr-root-mod? x n)
  (cond ((= x 1) #f)
        ((= x (- n 1)) #f)
        (else
         (if (= (remainder (square x) n) 1)
             #t
             #f))))

(nontrivial-sqr-root-mod? 17 144)
(nontrivial-sqr-root-mod? 3 8)
(nontrivial-sqr-root-mod? 8 63)
(nontrivial-sqr-root-mod? 8 72)

;; for cnt in 1 to n-1, is any 'cnt' the nontrivial square root of 1 mode n?
(define (has-nsrm? n)
  (define (nsrm-iter cnt n)
    (if (= cnt 1)
        false
        (if (nontrivial-sqr-root-mod? cnt n)
            true
            (nsrm-iter (- cnt 1) n))))
  (nsrm-iter (- n 1) n))

(has-nsrm? 144)
(has-nsrm? 5)
(has-nsrm? 4)

;; if the current modulo has nontrivial square root of 1 mod m,
;; return 0 and it will float back to the caller as 0 mode x will be
;; always 0
(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (if (has-nsrm? m)
             0
             (remainder (square (expmod base (/ exp 2) m))
                        m)))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))
;; if (expmod) returned 0 means we found a nontrivial square root and
;; n cannot be prime
(define (miller-rabin-test n)
  (define (try-it a)
    (cond ((= (expmod a n n) 0) false)
          (else (= (expmod a n n) a))))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
        ((miller-rabin-test n) (fast-prime? n (- times 1)))
        (else false)))


(fast-prime? 561 5) ;; carmichael
(fast-prime? 1105 5) ;; carmichael
(fast-prime? 1729 5) ;; carmichael
(fast-prime? 2465 5) ;; carmichael
(fast-prime? 2821 5) ;; carmichael
(fast-prime? 6601 5) ;; carmichael
(fast-prime? 2 5) ;; real prime
(fast-prime? 101 5) ;; real prime
(fast-prime? 10007 5) ;; real prime
(fast-prime? 100 5) ;; not prime
(fast-prime? 65535 5) ;; not prime


