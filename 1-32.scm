     ;; *Exercise 1.32:*
     ;;   a. Show that `sum' and `product' (*Note Exercise 1-31::) are
     ;;      both special cases of a still more general notion called
     ;;      `accumulate' that combines a collection of terms, using some
     ;;      general accumulation function:

     ;;           (accumulate combiner null-value term a next b)

     ;;      `Accumulate' takes as arguments the same term and range
     ;;      specifications as `sum' and `product', together with a
     ;;      `combiner' procedure (of two arguments) that specifies how
     ;;      the current term is to be combined with the accumulation of
     ;;      the preceding terms and a `null-value' that specifies what
     ;;      base value to use when the terms run out.  Write `accumulate'
     ;;      and show how `sum' and `product' can both be defined as
     ;;      simple calls to `accumulate'.

     ;;   b. If your `accumulate' procedure generates a recursive process,
     ;;      write one that generates an iterative process.  If it
     ;;      generates an iterative process, write one that generates a
     ;;      recursive process.


(define (inc x)
  (+ x 1))

(define (double x)
  (* 2 x))

(define (cube x) (* x x x))


(define (accumulate combiner null-value term-func a next-func b)
  (if (> a b)
      null-value
      (combiner (term-func a)
                (accumulate combiner
                            null-value
                            term-func
                            (next-func a)
                            next-func
                            b))))


(accumulate * 1 double 1 inc 5) ;; product, 3840
(accumulate + 0 cube 1 inc 3) ;; sum, 36

(define (accumulate-iter combiner null-value term-func a next-func b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next-func a) (combiner (term-func a) result))))
  (iter a null-value))


(accumulate-iter * 1 double 1 inc 5) ;; product, 3840
(accumulate-iter + 0 cube 1 inc 3) ;; sum, 36
