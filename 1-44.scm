(define (inc x)
  (+ x 1))

(define (square x)
  (* x x))


(define (compose f g)
  (lambda (x)
    (f (g x))))

(define (repeated f n)
  (if (= n 1)
      (lambda (x)
        (f x))
      (compose f (repeated f (- n 1)))))

(define (average a b c)
  (/ (+ a b c) 3))

(define (smooth f)
  (lambda (x)
    (define dx 0.000001)
    (average
     (f (- x dx))
     (f x)
     (f (+ x dx)))))

(define (n-fold-smooth f n)
  ((repeated smooth n) f))


(exact->inexact ((n-fold-smooth square 1) 5))
(exact->inexact ((n-fold-smooth square 2) 5))
(exact->inexact ((n-fold-smooth square 3) 5))
(exact->inexact ((n-fold-smooth square 12) 5))
    
    
  
