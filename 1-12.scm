(define (pascal row col)
  (cond ((= col 0) 1)
        ((= col row) 1)
        ((> col row) 0) ;; return 0 is error as Pascal Triangle has no 0
        (else (+ (pascal (- row 1) (- col 1)) (pascal (- row 1) col)))))


;;         1          <- Row 0
;;       1   1
;;     1   2   1
;;   1   3   3   1
;; 1   4   6   4   1  <- Row 4
;; ^   ^
;; |   |
;; 0   1 Col

;; 1 ]=> (pascal 0 1)

;; ;Value: 0

;; 1 ]=> (pascal 1 0)

;; ;Value: 1

;; 1 ]=> (pascal 2 1)

;; ;Value: 2

;; 1 ]=> (pascal 2 2)

;; ;Value: 1

;; 1 ]=> (pascal 4 1)

;; ;Value: 4

;; 1 ]=> (pascal 4 3)

;; ;Value: 4

;; 1 ]=> (pascal 5 3)

;; ;Value: 10
