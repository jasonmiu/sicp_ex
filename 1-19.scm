     ;; *Exercise 1.19:* There is a clever algorithm for computing the
     ;; Fibonacci numbers in a logarithmic number of steps.  Recall the
     ;; transformation of the state variables a and b in the `fib-iter'
     ;; process of section *Note 1-2-2::: a <- a + b and b <- a.  Call
     ;; this transformation T, and observe that applying T over and over
     ;; again n times, starting with 1 and 0, produces the pair _Fib_(n +
     ;; 1) and _Fib_(n).  In other words, the Fibonacci numbers are
     ;; produced by applying T^n, the nth power of the transformation T,
     ;; starting with the pair (1,0).  Now consider T to be the special
     ;; case of p = 0 and q = 1 in a family of transformations T_(pq),
     ;; where T_(pq) transforms the pair (a,b) according to a <- bq + aq +
     ;; ap and b <- bp + aq.  Show that if we apply such a transformation
     ;; T_(pq) twice, the effect is the same as using a single
     ;; transformation T_(p'q') of the same form, and compute p' and q' in
     ;; terms of p and q.  This gives us an explicit way to square these
     ;; transformations, and thus we can compute T^n using successive
     ;; squaring, as in the `fast-expt' procedure.  Put this all together
     ;; to complete the following procedure, which runs in a logarithmic
     ;; number of steps:(5)

     ;;      (define (fib n)
     ;;        (fib-iter 1 0 0 1 n))

     ;;      (define (fib-iter a b p q count)
     ;;        (cond ((= count 0) b)
     ;;              ((even? count)
     ;;               (fib-iter a
     ;;                         b
     ;;                         <??>      ; compute p'
     ;;                         <??>      ; compute q'
     ;;                         (/ count 2)))
     ;;              (else (fib-iter (+ (* b q) (* a q) (* a p))
     ;;                              (+ (* b p) (* a q))
     ;;                              p
     ;;                              q
     ;;                              (- count 1)))))


;; By definiation,
;; a <- bq + aq + ap
;; b <- bp + aq
;; Do the first step from T_(pq) to transform a_0 -> a_1 and b_0 -> b_1
;; 
;;
;; a_1 <- (b_0 * q) + (a_0 * q) + (a_0 * p)
;; b_1 <- (b_0 * p) + (a_0 * q)
;;
;; Apply T_(pq) again to obtain a_2, b_2
;;
;; a_2 <- (b_1 * q) + (a_1 * q) + (a_0 * p)
;; b_2 <- (b_1 * p) + (a_1 * q)
;; Substitute a_1 and b_1 in terms of a_0 and b_0. As we want to prove the
;; T_(p'q') is applying twice of T_(pq). We want to get the p' and q' in terms
;; or original p and q.
;;
;; a_2 <- (((b_0 * p) + (a_0 * q)) * q) + (((b_0 * q) + (a_0 * q) + (a_0 * p)) * q) + (((b_0 * q) + (a_0 * q) + (a_0 * p)) * p)
;; b_2 <- (((b_0 * p) + (a_0 * q)) * p) + (((b_0 * q) + (a_0 * q) + (a_0 * p)) * q)
;;
;; a_2 <- (a_0 * q^2) + (b_0 * p * q) + (b_0 * q^2) + (a_0 * q^2) + (a_0 * p * q) + (b_0 * p * q) + (a_0 * p * q) + (a_0 * p^ 2)
;; b_2 <- (b_0 * p^2) + (a_0 * p * q) + (b_0 * q^2) + (a_0 * q^2) + (a_0 * p * q)
;;
;; a_2 <- (2 * a_0 * q^2) + (2 * a_0 * p * q) + (2 * b_0 * p * q) + (a_0 * p^2) + (b_0 * q^2) = b_0 * q' + a_0 * q' + a_0 * p'
;; b_2 <- (a_0 * q^2) + (b_0 * p^2) + (b_0 * q^2) + (2 * a_0 * p * q) = b_0 * p' + a_0 * q'
;;
;; a_2 <- a_0 * p^2 + 2 * a_0 * q^2 + 2 * a_0 * p * q + b_0 * q^2 + 2 * b_0 * p * q
;; = a_0 * (p^2 + 2*q^2 + 2pq) + b_0 * (q^2 + 2pq)
;; = a_0 * (p' + q') + b_0 * q'
;; = a_0 * p' + a_0 * q' + b_0 * q' ## The T_(p'q') definiation
;; a_0 * (p^2 + 2*q^2 + 2pq) + b_0 * (q^2 + 2pq)
;;       \___ (p' + q')___/          \__ q' __/
;;
;; q' = q^2 + 2pq
;; p' = p^2 + q^2
;;
;; Verify on b_2
;; b_2 <- (a_0 * q^2) + (b_0 * p^2) + (b_0 * q^2) + (2 * a_0 * p * q)
;; = b_0 * p' + a_0 * q'
;; = (a_0 * q^2)  +   (2 * a_0 * p * q)   +  (b_0 * p^2)  +   (b_0 * q^2)
;;    \_ a_0 * q^2 + 2 * a_0 * p * q _/       \_ b_0 * p^2 + b_0 * q^2 _/
;;      \_a_0 * (q^2 + 2pq)                     \_ b_0 * (p^2 + q^2)
;;               \_ q'                                    \_ p'
;; = a_0 * q' + b_0 * p' ## The T_(p'q') definiation
;; Hence,
;; p' = p^2 + q^2
;; q' = q^2 + 2pq


(define (fib n)
  (fib-iter 1 0 0 1 n))

(define (fib-iter a b p q count)
  (cond ((= count 0) b)
        ((even? count)
         (fib-iter a
                   b
                   (+ (* p p ) (* q q))      ; p' = p^2 + q^2
                   (+ (* q q) (* 2 p q))     ; q' = q^2 + 2pg
                   (/ count 2)))
        (else (fib-iter (+ (* b q) (* a q) (* a p))
                        (+ (* b p) (* a q))
                        p
                        q
                        (- count 1)))))

(fib 0)
(fib 1)
(fib 2)
(fib 3)
(fib 4)
(fib 5)
(fib 6)
(fib 7)
(fib 8)
(fib 9)
(fib 10)
(fib 11)
(fib 12)
(fib 150) ;; 9969216677189303386214405760200

;; Fib 1 -12 :  0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144
;; Fib 150: 9969216677189303386214405760200
;; http://www.maths.surrey.ac.uk/hosted-sites/R.Knott/Fibonacci/fibtable.html
