     ;; *Exercise 1.29:* Simpson's Rule is a more accurate method of
     ;; numerical integration than the method illustrated above.  Using
     ;; Simpson's Rule, the integral of a function f between a and b is
     ;; approximated as

     ;;      h
     ;;      - (y_0 + 4y_1 + 2y_2 + 4y_3 + 2y_4 + ... + 2y_(n-2) + 4y_(n-1) + y_n)
     ;;      3

     ;; where h = (b - a)/n, for some even integer n, and y_k = f(a + kh).
     ;; (Increasing n increases the accuracy of the approximation.)
     ;; Define a procedure that takes as arguments f, a, b, and n and
     ;; returns the value of the integral, computed using Simpson's Rule.
     ;; Use your procedure to integrate `cube' between 0 and 1 (with n =
     ;; 100 and n = 1000), and compare the results to those of the
     ;; `integral' procedure shown above.


;; (define (<NAME> a b)
;;   (if (> a b)
;;       0
;;       (+ (<TERM> a)
;;          (<NAME> (<NEXT> a) b))))


(define (cube x) (* x x x))

(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))


(define (even? x)
  (if (= (remainder x 2) 0)
      true
      false))
(define (inc x)
  (+ x 1))
  

(define (simpson-integral f a b n)
  (define (ck k)
    (cond ((or (= k 0) (= k n)) 1)
          ((even? k) 2)
          (else 4)))
  (define (yk f a b n k)
    (f (+ a (* k (/ (- b a) n)))))
  (define (simpson-term k)
    (* (ck k) (yk f a b n k)))
  (* (/ (/ (- b a) n) 3)
     (sum simpson-term 0 inc n)))

;; y_k = f(a + kh)
;;         1 if k = 0 or k = n;
;; c_k   = 2 if k is even
;;         4 if k is odd
;; h = (b - a) / n
;; simpson-term(k) = y_k * c_k
;; (y_0 +    4y_1 +    2y_2 + 4y_3 + 2y_4 + ... + 2y_(n-2) + 4y_(n-1) + y_n)
;;  \        \         \
;;   \y_0c_0  \y_1c_1   \y_2c_2
;;
;; 'a' and 'b' are const from simpon-intergral(), we want to increment the k,
;; the term index up to n, ie:
;;
;;  n
;; ---
;; >   f(k) = y_0c_0 + y_1c_1 + ... + y_kc_k + ... + y_nc_n
;; ---
;;  0
;; The 'k' in the simpson-term() becomes 'a' of sum() (start index of summation)
;; , and 'n' becomes the 'b' of sum() (end index of summation)


(simpson-integral cube 0 1 100)

(simpson-integral cube 0 1 1000)
  
