     ;; *Exercise 1.33:* You can obtain an even more general version of
     ;; `accumulate' (*Note Exercise 1-32::) by introducing the notion of
     ;; a "filter" on the terms to be combined.  That is, combine only
     ;; those terms derived from values in the range that satisfy a
     ;; specified condition.  The resulting `filtered-accumulate'
     ;; abstraction takes the same arguments as accumulate, together with
     ;; an additional predicate of one argument that specifies the filter.
     ;; Write `filtered-accumulate' as a procedure.  Show how to express
     ;; the following using `filtered-accumulate':

     ;;   a. the sum of the squares of the prime numbers in the interval a
     ;;      to b (assuming that you have a `prime?' predicate already
     ;;      written)

     ;;   b. the product of all the positive integers less than n that are
     ;;      relatively prime to n (i.e., all positive integers i < n such
     ;;      that GCD(i,n) = 1).




(define (gcd a b)
  (if (= b 0)
      a
      (gcd b (remainder a b))))

(define (relatively-prime? a b)
  (if (= (gcd a b) 1)
      true
      false))

(define (print-relatively-primes n)
  (define (iter i)
    (if (relatively-prime? i n)
        (begin
          (display i)
          (newline)))
    (if (< i n)
        (iter (+ i 1))
        true))
  (iter 1))

(print-relatively-primes 20)
        
;;;; Prime test from 1-28

(define (square x)
  (* x x))

(define (even? x)
  (= (remainder x 2) 0))

;; Check if x is the nontrivial square root of 1 mod n
;; ie. if (x^2) mod n == 1 mod n, x is nontrivial square root of 1 mod n
(define (nontrivial-sqr-root-mod? x n)
  (cond ((= x 1) #f)
        ((= x (- n 1)) #f)
        (else
         (if (= (remainder (square x) n) 1)
             #t
             #f))))

;; for cnt in 1 to n-1, is any 'cnt' the nontrivial square root of 1 mode n?
(define (has-nsrm? n)
  (define (nsrm-iter cnt n)
    (if (= cnt 1)
        false
        (if (nontrivial-sqr-root-mod? cnt n)
            true
            (nsrm-iter (- cnt 1) n))))
  (nsrm-iter (- n 1) n))

;; if the current modulo has nontrivial square root of 1 mod m,
;; return 0 and it will float back to the caller as 0 mode x will be
;; always 0
(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (if (has-nsrm? m)
             0
             (remainder (square (expmod base (/ exp 2) m))
                        m)))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))

;; if (expmod) returned 0 means we found a nontrivial square root and
;; n cannot be prime
(define (miller-rabin-test n)
  (define (try-it a)
    (cond ((= (expmod a n n) 0) false)
          (else (= (expmod a n n) a))))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
        ((miller-rabin-test n) (fast-prime? n (- times 1)))
        (else false)))

(define (prime? n)
  (fast-prime? n 20))

;;;; 1.33

(define (self x)
  x)

(define (inc x)
  (+ x 1))


(define (accumulate-filter-iter combiner null-value
                                term-func a next-func b
                                predicate)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next-func a)
              (combiner
               (if (predicate a)
                   (term-func a)
                   null-value)
               result))))
  (iter a null-value))

(accumulate-filter-iter + 0 self 2 inc 10 prime?) ; 17

(define (relatively-prime-to-20? x)
  (if (relatively-prime? x 20)
      true
      false))

(accumulate-filter-iter * 1 self 2 inc 20 relatively-prime-to-20?) ; 8729721

                        
