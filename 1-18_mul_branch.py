import sys

i = int(sys.argv[1])
j = int(sys.argv[2])

c = 0
while i < j:
    k = i * j
    c = c + 1
    i = i + 1
    j = j - 1

print(c)

# On my 2013 Macbook Pro
# $ time python3 ./1-18_mul_branch.py  0 400000
# 200000

# real	0m0.168s
# user	0m0.137s
# sys	0m0.019s


