     ;; *Exercise 1.22:* Most Lisp implementations include a primitive
     ;; called `runtime' that returns an integer that specifies the amount
     ;; of time the system has been running (measured, for example, in
     ;; microseconds).  The following `timed-prime-test' procedure, when
     ;; called with an integer n, prints n and checks to see if n is
     ;; prime.  If n is prime, the procedure prints three asterisks
     ;; followed by the amount of time used in performing the test.

     ;;      (define (timed-prime-test n)
     ;;        (newline)
     ;;        (display n)
     ;;        (start-prime-test n (runtime)))

     ;;      (define (start-prime-test n start-time)
     ;;        (if (prime? n)
     ;;            (report-prime (- (runtime) start-time))))

     ;;      (define (report-prime elapsed-time)
     ;;        (display " *** ")
     ;;        (display elapsed-time))

     ;; Using this procedure, write a procedure `search-for-primes' that
     ;; checks the primality of consecutive odd integers in a specified
     ;; range.  Use your procedure to find the three smallest primes
     ;; larger than 1000; larger than 10,000; larger than 100,000; larger
     ;; than 1,000,000.  Note the time needed to test each prime.  Since
     ;; the testing algorithm has order of growth of [theta](_[sqrt]_(n)),
     ;; you should expect that testing for primes around 10,000 should
     ;; take about _[sqrt]_(10) times as long as testing for primes around
     ;; 1000.  Do your timing data bear this out?  How well do the data
     ;; for 100,000 and 1,000,000 support the _[sqrt]_(n) prediction?  Is
     ;; your result compatible with the notion that programs on your
     ;; machine run in time proportional to the number of steps required
     ;; for the computation?

(define (square x)
  (* x x))

(define (even? x)
  (= (remainder x 2) 0))

;; Fast prime test with Fermat test
(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else false)))


;; Basic prime test
(define (smallest-divisor n)
  (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))

(define (divides? a b)
  (= (remainder b a) 0))


                                        ;(define (prime? x)
                                        ;  (fast-prime? x 10))



;; Machine is too fast the runtime resolution is too low
;; to find the difference. Force to slow down.
(define (prime? x)
  (define (slow-down x n)
    (= x (smallest-divisor x))
    (if (> n 0)
        (slow-down x (- n 1))
        (= x (smallest-divisor x))))
  (slow-down x 10000))

        
(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))
      false))
      
(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time)
  true)


(timed-prime-test 17)
(start-prime-test 10 0)

(define (search-for-primes start count)
  (if (> count 0)
      (if (even? start)
          (search-for-primes (+ 1 start) count)
          (if (timed-prime-test start)
              (search-for-primes (+ 2 start) (- count 1))
              (search-for-primes (+ 2 start) count)))
      true))
          

(search-for-primes 10 1)
(search-for-primes 10 2)
(search-for-primes 10 3)

(search-for-primes 1000 3)
(search-for-primes 10000 3)
(search-for-primes 100000 3)
(search-for-primes 1000000 3)
        
;; n           time
;; 1000        0.4s
;; 10000       1.28
;; 100000      4
;; 1000000     12.9
;;
;; sqrt(1000) = 31.62, ratio to time = 79.5
;; sqrt(10000) = 100, ratio to time = 78.125
;; sqrt(100000) = 316.22, ratio to time = 79
;; sqrt(1000000) = 1000.0, ratio to time = 77.52
;; So the complexity = O(sqrt(n)), which is bound by the
;; find-divisor() as it has sqrt(n) steps

  
;; 1 ]=> (search-for-primes 1000 3)
;; 1001
;; 1003
;; 1005
;; 1007
;; 1009 *** .3999999999999999
;; 1011
;; 1013 *** .4099999999999999
;; 1015
;; 1017
;; 1019 *** .40000000000000036
;; ;Value: #t

;; 1 ]=> (search-for-primes 10000 3)
;; 10001
;; 10003
;; 10005
;; 10007 *** 1.29
;; 10009 *** 1.2799999999999994
;; 10011
;; 10013
;; 10015
;; 10017
;; 10019
;; 10021
;; 10023
;; 10025
;; 10027
;; 10029
;; 10031
;; 10033
;; 10035
;; 10037 *** 1.2799999999999994
;; ;Value: #t

;; 1 ]=> (search-for-primes 100000 3)
;; 100001
;; 100003 *** 4.029999999999999
;; 100005
;; 100007
;; 100009
;; 100011
;; 100013
;; 100015
;; 100017
;; 100019 *** 4.02
;; 100021
;; 100023
;; 100025
;; 100027
;; 100029
;; 100031
;; 100033
;; 100035
;; 100037
;; 100039
;; 100041
;; 100043 *** 4.050000000000001
;; ;Value: #t

;; 1 ]=> (search-for-primes 1000000 3)
;; 1000001
;; 1000003 *** 12.870000000000001
;; 1000005
;; 1000007
;; 1000009
;; 1000011
;; 1000013
;; 1000015
;; 1000017
;; 1000019
;; 1000021
;; 1000023
;; 1000025
;; 1000027
;; 1000029
;; 1000031
;; 1000033 *** 12.829999999999998
;; 1000035
;; 1000037 *** 12.960000000000008
;; ;Value: #t
