first eval 'define' which is a special form.
First list (a-plus-abs-b a b) is a function with name "a-plus-abs-b" and two arguments "a" and "b".
Next list is the function body.
Then eval "if", which is a special form too.
The first list of if (> b 0) is the condition. if "b larger than 0, then return "+" else, return "-".
This returned result will subsitute to the next list (+/- a b). Depends on the returned value of "if" expression,
+ (when b is a positive number) or - (when b is a netgative number) will be eval to (+ a b) or (- a b).
So the net result of this compound expression is defining a function which compute the sum of the value a and the absolute value of b.
