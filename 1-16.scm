     ;; *Exercise 1.16:* Design a procedure that evolves an iterative
     ;; exponentiation process that uses successive squaring and uses a
     ;; logarithmic number of steps, as does `fast-expt'.  (Hint: Using the
     ;; observation that (b^(n/2))^2 = (b^2)^(n/2), keep, along with the
     ;; exponent n and the base b, an additional state variable a, and
     ;; define the state transformation in such a way that the product a
     ;; b^n is unchanged from state to state.  At the beginning of the
     ;; process a is taken to be 1, and the answer is given by the value
     ;; of a at the end of the process.  In general, the technique of
     ;; defining an "invariant quantity" that remains unchanged from state
     ;; to state is a powerful way to think about the design of iterative
     ;; algorithms.)


(define (even? x)
  (= (remainder x 2) 0))

(define (square x)
  (* x x))

;; Use the counter c to count for n/2 times of b^2 multiplication.
;; Base on the observation if n is even:
;; b^n = (b^(n/2))^2 = (b^2)^(n/2)
;; b^n =
;; (b^2) * (b^2) * ... * (b^2)
;; \_______  n/2 ___________/
;; and a = a * b^2 for answer accumulation. When we have n/2 times of
;; multiplication, return a
;; If n is odd then we it is b^n = b * b^(n-1),
;; Need eval the b^(n-1) first and make it become recursive not fully
;; iterative.

(define (expt-iter_c b n a c)
  (cond ((= n 0) 1)
        ((= n 1) b)
        ((= (- (/ n 2) c) 0) a)
        (else
         (if (even? n)
             (expt-iter_c b n (* a (square b)) (+ c 1))
             (* b (expt-iter_c b (- n 1) a c))))))

(define (fast-expt-with-counter b n)
  (expt-iter b n 1 0))


;; Better solution. Accumulate the product of b^2 on argument b.
;; For each loop (call expt-iter()), current b_i is the (b_i-1)^2,
;; Loop n/2 times. When n = 1, multiply the b and a and put it to
;; a as result to return when n = 0 (last loop).
;; By doing this we are keeping a * (b^n) as loop invariant.
;; Example:
;; Inputs of the beginning of every loop
;; b  n  a   ab^n
;; 2  4  1   16
;; 4  2  1   16
;; 16 1  1   16
;; 16 0  16  16  <-- n = 0, return a
;;
;; We transferred the b^n to the variable a as the result we want

(define (expt-iter b n a)
  (cond ((= n 0) a)
        (else
         (if (even? n)
             (expt-iter (square b) (/ n 2) a)
             (expt-iter b (- n 1) (* b a))))))

(define (fast-expt b n)
  (expt-iter b n 1))
