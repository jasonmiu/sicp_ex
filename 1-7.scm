;; scheme  --load 1-7.scm

(define (sqrt-iter guess x)
    (if (good-enough? guess x)
        guess
        (sqrt-iter (improve guess x)
                   x)))

(define (improve guess x)
    (average guess (/ x guess)))

(define (average x y)
    (/ (+ x y) 2))


;; want to know if 'guess' is close to the anwser, which is
;; the real square root of x. Since guess^2 ~= real-root^2 == x,
;; we square the 'guess' and check its difference between x.
(define (good-enough? guess x )
        (< (abs (- (square guess) x)) 0.001))

(define (abs x)
    (if (< x 0)
        (- x)
        x))

(define (square x)
    (* x x))

(define (sqrt x)
    (sqrt-iter 1.0 x))


;; If the x is very small, like 0.0001, the corret sqrt(0.0001) is 0.01 while 
;; (sqrt 0.0001) gives .0323. It is because 0.0323^2 is ~= 0.00104 the different
;; between this and x is smaller than threshold 0.001 already. The actual anwser
;; is so small the thresold 0.001 compare to it is too big and hence the program
;; stops too early.
;; If the x is very large, the machine preceision may not enough to represent
;; small different between two large numbers. It runs (sqrt-iter) again
;; as the different between guess and x will never be within the threshold 0.001
;; On MacBook Pro 64bits Inter CPU, (sqrt 100000000000000000000000000000)
;; causes infinite loop.


;; A better way is adjust the threshold as the fraction of the difference
;; between the current guess and last guess, to keep check the change of each
;; iteration


(define (sqrt-iter2 guess prev-guess x)
    (if (good-enough2? guess prev-guess x)
        guess
        (sqrt-iter2 (improve guess x)
                    guess
                    x)))

(define (good-enough2? guess prev-guess x)
  (< (abs (- guess prev-guess)) (/ guess 10000.0)))

(define (sqrt2 x)
    (sqrt-iter2 1.0 0.0 x))


;; 1 ]=> (sqrt 100000000000000000000000000000)
;; ^C
;; Interrupt option (? for help):
;; ;Quit!

;; 1 ]=> (sqrt2 100000000000000000000000000000)

;; ;Value: 316227766017107.6

