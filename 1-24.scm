     ;; *Exercise 1.24:* Modify the `timed-prime-test' procedure of *Note
     ;; Exercise 1-22:: to use `fast-prime?' (the Fermat method), and test
     ;; each of the 12 primes you found in that exercise.  Since the
     ;; Fermat test has [theta](`log' n) growth, how would you expect the
     ;; time to test primes near 1,000,000 to compare with the time needed
     ;; to test primes near 1000?  Do your data bear this out?  Can you
     ;; explain any discrepancy you find?


(define (square x)
  (* x x))

(define (even? x)
  (= (remainder x 2) 0))

;; Fast prime test with Fermat test
(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else false)))


;; Machine is too fast the runtime resolution is too low
;; to find the difference. Force to slow down.
(define (prime? x)
  (define (slow-down x n)
    (fast-prime? x 10)
    (if (> n 0)
        (slow-down x (- n 1))
        (fast-prime? x 10)))
  (slow-down x 10000))

(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))
      false))
      
(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time)
  true)


(timed-prime-test 17)
(start-prime-test 10 0)

(define (search-for-primes start count)
  (if (> count 0)
      (if (even? start)
          (search-for-primes (+ 1 start) count)
          (if (timed-prime-test start)
              (search-for-primes (+ 2 start) (- count 1))
              (search-for-primes (+ 2 start) count)))
      true))
          

(search-for-primes 10 1)
(search-for-primes 10 2)
(search-for-primes 10 3)

(search-for-primes 1000 3)
(search-for-primes 10000 3)
(search-for-primes 100000 3)
(search-for-primes 1000000 3)

;; 1 ]=> (search-for-primes 10 3)
;; 11 *** 1.12
;; 13 *** 1.1099999999999994
;; 15
;; 17 *** 1.1600000000000001
;; ;Value: #t

;; 1 ]=> (search-for-primes 1000 3)
;; 1001
;; 1003
;; 1005
;; 1007
;; 1009 *** 2.619999999999999
;; 1011
;; 1013 *** 2.74
;; 1015
;; 1017
;; 1019 *** 2.870000000000001
;; ;Value: #t

;; 1 ]=> (search-for-primes 10000 3)
;; 10001
;; 10003
;; 10005
;; 10007 *** 3.3699999999999974
;; 10009 *** 3.240000000000002
;; 10011
;; 10013
;; 10015
;; 10017
;; 10019
;; 10021
;; 10023
;; 10025
;; 10027
;; 10029
;; 10031
;; 10033
;; 10035
;; 10037 *** 3.370000000000001
;; ;Value: #t

;; 1 ]=> (search-for-primes 100000 3)
;; 100001
;; 100003 *** 3.8599999999999994
;; 100005
;; 100007
;; 100009
;; 100011
;; 100013
;; 100015
;; 100017
;; 100019 *** 3.969999999999999
;; 100021
;; 100023
;; 100025
;; 100027
;; 100029
;; 100031
;; 100033
;; 100035
;; 100037
;; 100039
;; 100041
;; 100043 *** 3.960000000000001
;; ;Value: #t

;; 1 ]=> (search-for-primes 1000000 3)
;; 1000001
;; 1000003 *** 4.460000000000001
;; 1000005
;; 1000007
;; 1000009
;; 1000011
;; 1000013
;; 1000015
;; 1000017
;; 1000019
;; 1000021
;; 1000023
;; 1000025
;; 1000027
;; 1000029
;; 1000031
;; 1000033 *** 4.460000000000008
;; 1000035
;; 1000037 *** 4.569999999999993
;; ;Value: #t

;; 11 *** 1.12
;; 13 *** 1.1099999999999994
;; 17 *** 1.1600000000000001
;; 1009 *** 2.619999999999999
;; 1013 *** 2.74
;; 1019 *** 2.870000000000001
;; 10007 *** 3.3699999999999974
;; 10009 *** 3.240000000000002
;; 10037 *** 3.370000000000001
;; 100003 *** 3.8599999999999994
;; 100019 *** 3.969999999999999
;; 100043 *** 3.960000000000001
;; 1000003 *** 4.460000000000001
;; 1000033 *** 4.460000000000008
;; 1000037 *** 4.569999999999993

;; Avg run time
; 2.7433333333333336 ; 1000 
; 3.3266666666666667 ; 10000
; 3.9299999999999997 ; 100000
; 4.496666666666667  ; 1000000

;; Ratio of runtime with different input size
;; (/ 2.743 3.327) ; 0.824
;; (/ 3.327 3.93) ; 0.846
;; (/ 3.93 4.49) ; 0.875

;; Ratio of O(logn) growth
;; (/ (log 1000) (log 10000)) ; 0.75
;; (/ (log 10000) (log 100000)) ; 0.8
;; (/ (log 100000) (log 1000000)) ; 0.833

;; The actual runtime growth is close to the O(logn) growth, which is about 0.8.
