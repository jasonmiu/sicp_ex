;; *Exercise 1.35:* Show that the golden ratio [phi] (section *Note
;; 1-2-2::) is a fixed point of the transformation x |-> 1 + 1/x, and
;; use this fact to compute [phi] by means of the `fixed-point'
;; procedure.


;; Golden Ratio 'phi' has to satisfies this equation:
;; [phi]^2 = [phi] + 1
;; let x = [phi]
;; x^2 = x + 1
;; (x^2) / x = (x + 1) / x
;; x = 1 + 1/x
;; so
;; f(x) = 1 + 1/x
;; and if f(x) = x (fixed point) the x is the golden ratio

(define tolerance 0.00001)

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))


(define (golden)
  (fixed-point (lambda (x) (+ 1 (/ 1 x))) 1.0))

(golden)
  


