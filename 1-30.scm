     ;; *Exercise 1.30:* The `sum' procedure above generates a linear
     ;; recursion.  The procedure can be rewritten so that the sum is
     ;; performed iteratively.  Show how to do this by filling in the
     ;; missing expressions in the following definition:

     ;;      (define (sum term a next b)
     ;;        (define (iter a result)
     ;;          (if <??>
     ;;              <??>
     ;;              (iter <??> <??>)))
     ;;        (iter <??> <??>))


(define (cube x) (* x x x))

(define (sum term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (+ (term a) result))))
  (iter a 0))


(define (even? x)
  (if (= (remainder x 2) 0)
      true
      false))
(define (inc x)
  (+ x 1))
  

(define (simpson-integral f a b n)
  (define (ck k)
    (cond ((or (= k 0) (= k n)) 1)
          ((even? k) 2)
          (else 4)))
  (define (yk f a b n k)
    (f (+ a (* k (/ (- b a) n)))))
  (define (simpson-term k)
    (* (ck k) (yk f a b n k)))
  (* (/ (/ (- b a) n) 3)
     (sum simpson-term 0 inc n)))

(sum cube 1 inc 3) ; 36


(simpson-integral cube 0 1 100)

(simpson-integral cube 0 1 1000)
  
