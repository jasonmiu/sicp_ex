Application evaluation:
0
First subsitute (test x y) -> (test 0 (p))
then
(if (= 0 0)
         0
         (p)),
and eval (if), since the condition 0 == 0 matched, it returns 0.



Normal evaluation:
infinite loop
First subsitute (test x y) -> (test 0 (p))
then
(if (= 0 0)
         0
         (((((( ... p ))))))
         ))
will try to subsitute the (p) with (define (p) (p)) recursively. Hence will
loop forever until the machine exhausted memory.
