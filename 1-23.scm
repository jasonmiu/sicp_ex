     ;; *Exercise 1.23:* The `smallest-divisor' procedure shown at the
     ;; start of this section does lots of needless testing: After it
     ;; checks to see if the number is divisible by 2 there is no point in
     ;; checking to see if it is divisible by any larger even numbers.
     ;; This suggests that the values used for `test-divisor' should not
     ;; be 2, 3, 4, 5, 6, ..., but rather 2, 3, 5, 7, 9, ....  To
     ;; implement this change, define a procedure `next' that returns 3 if
     ;; its input is equal to 2 and otherwise returns its input plus 2.
     ;; Modify the `smallest-divisor' procedure to use `(next
     ;; test-divisor)' instead of `(+ test-divisor 1)'.  With
     ;; `timed-prime-test' incorporating this modified version of
     ;; `smallest-divisor', run the test for each of the 12 primes found in
     ;; *Note Exercise 1-22::.  Since this modification halves the number
     ;; of test steps, you should expect it to run about twice as fast.
     ;; Is this expectation confirmed?  If not, what is the observed ratio
     ;; of the speeds of the two algorithms, and how do you explain the
     ;; fact that it is different from 2?

(define (square x)
  (* x x))

(define (even? x)
  (= (remainder x 2) 0))

;; Basic prime test
(define (smallest-divisor n)
  (find-divisor n 2))

(define (next x)
  (if (= x 2)
      3
      (+ x 2)))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (next test-divisor)))))



(define (divides? a b)
  (= (remainder b a) 0))


;; Machine is too fast the runtime resolution is too low
;; to find the difference. Force to slow down.
(define (prime? x)
  (define (slow-down x n)
    (= x (smallest-divisor x))
    (if (> n 0)
        (slow-down x (- n 1))
        (= x (smallest-divisor x))))
  (slow-down x 10000))

        
(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))
      false))
      
(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time)
  true)


(timed-prime-test 17)
(start-prime-test 10 0)

(define (search-for-primes start count)
  (if (> count 0)
      (if (even? start)
          (search-for-primes (+ 1 start) count)
          (if (timed-prime-test start)
              (search-for-primes (+ 2 start) (- count 1))
              (search-for-primes (+ 2 start) count)))
      true))
          

(search-for-primes 10 1)
(search-for-primes 10 2)
(search-for-primes 10 3)

(search-for-primes 1000 3)
(search-for-primes 10000 3)
(search-for-primes 100000 3)
(search-for-primes 1000000 3)

;; 1 ]=> (search-for-primes 100000 3)
;; 100001
;; 100003 *** 2.4699999999999998
;; 100005
;; 100007
;; 100009
;; 100011
;; 100013
;; 100015
;; 100017
;; 100019 *** 2.5700000000000003
;; 100021
;; 100023
;; 100025
;; 100027
;; 100029
;; 100031
;; 100033
;; 100035
;; 100037
;; 100039
;; 100041
;; 100043 *** 2.460000000000001
;; ;Value: #t

;; 1 ]=> (search-for-primes 1000000 3)
;; 1000001
;; 1000003 *** 7.890000000000001
;; 1000005
;; 1000007
;; 1000009
;; 1000011
;; 1000013
;; 1000015
;; 1000017
;; 1000019
;; 1000021
;; 1000023
;; 1000025
;; 1000027
;; 1000029
;; 1000031
;; 1000033 *** 7.740000000000002
;; 1000035
;; 1000037 *** 7.780000000000001
;; ;Value: #t

;; Speed up:
(/ (+ (/  4.029999999999999 2.4699999999999998)
(/  4.02 2.5700000000000003)
(/  4.050000000000001 2.460000000000001)
(/  12.870000000000001 7.890000000000001)
(/  12.829999999999998 7.740000000000002)
(/  12.960000000000008 7.780000000000001))
   6)

;; about 1.632 times
;; It is not enough 2 times speed up. I believe its because the overhead of
;; calling a function next(), which is slower than primitive operation '+'

