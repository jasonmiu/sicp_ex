(define (f n)
  (cond ((< n 3) n)
        (else
         (+ (f (- n 1)) (* 2 (f (- n 2))) (* 3 (f (- n 3)))))))

;; f(n) = f(a) + 2 * f(b) + 3 * f(c), where a = n-1, b = n-2, c = n-3
;; f(n+1) = f(n) + 2 * f(n-1) + 3 * f(n-2)
;; so :
;; a <- a + 2 * b + 2 * c
;; b <- a
;; c <- b

(define (f2 n)
  (f2-iter 2 1 0 n))

(define (f2-iter a b c count)
  (cond ((= count 2) a)
        ((= count 1) b)
        ((= count 0) c)
        (else
         (f2-iter (+ a (* 2 b) (* 3 c))
                  a
                  b
                  (- count 1)))))
  
