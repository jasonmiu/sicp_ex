;; *Exercise 1.8:* Newton's method for cube roots is based on the
;; fact that if y is an approximation to the cube root of x, then a
;; better approximation is given by the value

;;      x/y^2 + 2y
;;      ----------
;;          3

(define (cube-root-iter guess prev-guess x)
  (if (good-enough? guess prev-guess)
      guess
      (cube-root-iter (improve x guess) guess x)))

(define (good-enough? guess prev-guess)
  (< (abs (- guess prev-guess)) (/ guess 10000.0)))

(define (abs x)
  (if (< x 0)
      (- x)
      x))

;; Use the approximation above
(define (improve x y)
  (/ (+ (/ x (* y y)) (* 2 y)) 3))

;; handle the -ve x case
;; Check real root / complex root discussion (in python)
;; https://stackoverflow.com/questions/30923838/how-to-get-the-real-cube-root-of-a-negative-number-in-python3?rq=1
(define (cube-root x)
  (if (< x 0)
      (- (cube-root-iter 1.0 0.0 (abs x)))
      (cube-root-iter 1.0 0.0 x)))

