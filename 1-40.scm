     ;; *Exercise 1.40:* Define a procedure `cubic' that can be used
     ;; together with the `newtons-method' procedure in expressions of the
     ;; form

     ;;      (newtons-method (cubic a b c) 1)

     ;; to approximate zeros of the cubic x^3 + ax^2 + bx + c.


(define tolerance 0.00001)

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))


(define (deriv g)
  (lambda (x)
    (/ (- (g (+ x dx)) (g x))
       dx)))

(define dx 0.00001)

(define (newton-transform g)
  (lambda (x)
    (- x (/ (g x) ((deriv g) x)))))

(define (newtons-method g guess)
  (fixed-point (newton-transform g) guess))

(define (fixed-point-of-transform g transform guess)
  (fixed-point (transform g) guess))


(define (sqrt x)
  (newtons-method (lambda (y) (- (square y) x))
                  1.0))


(sqrt 100)
(sqrt 35)

; return cubic as a f(x) = x^3 + ax^2 + bx +c
(define (cubic a b c)
  (lambda (x)
    (+ (* x x x) (* x x a)  (* x b) c)))

(newtons-method (cubic 1 1 1) 1)

(newtons-method (cubic 2 1 10) 1)
