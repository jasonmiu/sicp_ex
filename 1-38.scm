     ;; *Exercise 1.38:* In 1737, the Swiss mathematician Leonhard Euler
     ;; published a memoir `De Fractionibus Continuis', which included a
     ;; continued fraction expansion for e - 2, where e is the base of the
     ;; natural logarithms.  In this fraction, the n_i are all 1, and the
     ;; D_i are successively 1, 2, 1, 1, 4, 1, 1, 6, 1, 1, 8, ....  Write
     ;; a program that uses your `cont-frac' procedure from *Note Exercise
     ;; 1-37:: to approximate e, based on Euler's expansion.

(define (d_term i)
  (cond ((= i 1) 1)
        ((= i 2) 2)
        ((and (> i 2) (= (remainder (- i 2) 3) 0))
         ( + (* (/ (- i 2) 3) 2) 2))
        (else 1)))

(define (loop_d i n)
  (newline)
  (display (d_term i))
  (if (<= i n)
      (loop_d (+ i 1) n)))

(loop_d 1 13)

(define (cont-frac-iter n d k)
  (define (iter i ans)
    (if (= i 0)
        ans
        (if (= i k)
            (iter (- i 1) (/ (n i) (d i)))
            (iter (- i 1) (/ (n i) (+ ans (d i)))))))
  (iter k 0))

;; Euler expansion
(cont-frac-iter (lambda (i) 1.0)
                d_term
                12)

;; e - 2, should be equal to Euler expansion
(- 2.71828 2)



      
   

