     ;; *Exercise 1.18:* Using the results of *Note Exercise 1-16:: and
     ;; *Note Exercise 1-17::, devise a procedure that generates an
     ;; iterative process for multiplying two integers in terms of adding,
     ;; doubling, and halving and uses a logarithmic number of steps.(4)

;; Reuse the iterative algorithm we did in 1-17

(define (double x)
  (+ x x))

(define (halve x)
  (/ x 2))

(define (even? x)
  (= (remainder x 2) 0))


(define (mul-iter a b c)
  (cond ((= a 0) c)
        (else
         (if (even? a)
             (mul-iter (halve a) (double b) c)
             (mul-iter (- a 1) b (+ b c))))))

(define (mul a b)
  (mul-iter a b 0))


;; a branch mark loop, So multiplication (i + j) / 2 times,
;; while i * j -> i+1 * j-1 -> ...
(define (loop-mul i j c)
  (mul i j)
  (if (< i j)
      (loop-mul (+ i 1) (- j 1) (+ c 1))
      c))

(loop-mul 0 400000 0)

;; On my 2013 MacBook pro
;; 1 ]=> (loop-mul 0 400000 0)
;; ;Value: 200000

;; 1 ]=>
;; End of input stream reached.
;; Moriturus te saluto.

;; real	0m7.265s
;; user	0m6.992s
;; sys	0m0.167s

;; Native Python multiplication
;; $ time python3 ./1-18_mul_branch.py  0 400000
;; 200000

;; real	0m0.168s
;; user	0m0.137s
;; sys	0m0.019s
