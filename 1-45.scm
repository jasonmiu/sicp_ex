     ;; *Exercise 1.45:* We saw in section *Note 1-3-3:: that attempting
     ;; to compute square roots by naively finding a fixed point of y |->
     ;; x/y does not converge, and that this can be fixed by average
     ;; damping.  The same method works for finding cube roots as fixed
     ;; points of the average-damped y |-> x/y^2.  Unfortunately, the
     ;; process does not work for fourth roots--a single average damp is
     ;; not enough to make a fixed-point search for y |-> x/y^3 converge.
     ;; On the other hand, if we average damp twice (i.e., use the average
     ;; damp of the average damp of y |-> x/y^3) the fixed-point search
     ;; does converge.  Do some experiments to determine how many average
     ;; damps are required to compute nth roots as a fixed-point search
     ;; based upon repeated average damping of y |-> x/y^(n-1).  Use this
     ;; to implement a simple procedure for computing nth roots using
     ;; `fixed-point', `average-damp', and the `repeated' procedure of
     ;; *Note Exercise 1-43::.  Assume that any arithmetic operations you
     ;; need are available as primitives.


(define tolerance 0.00001)

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (fixed-point-of-transform g transform guess)
  (fixed-point (transform g) guess))

(define (average a b)
  (/ (+ a b) 2))

(define (average-damp f)
  (lambda (x) 
    (average x (f x))))

(define (compose f g)
  (lambda (x)
    (f (g x))))


(define (repeated f n)
  (if (= n 1)
      (lambda (x)
        (f x))
      (compose f (repeated f (- n 1)))))


(define (pow x n)
  (cond ((= n 0) 1)
        ((= n 1) x)
        (else
         (* x (pow x (- n 1))))))


(pow 2 0)
(pow 2 1)
(pow 2 10)

     
(define (nth-root-damped x nth damp)
  (fixed-point
    ((repeated average-damp damp)
    (lambda (y)
      (/ x (pow y (- nth 1)))))
   1.0))

(nth-root-damped 2 5 4)


;; By varying the parameters, we can see for x=2x=2 the minimal number of
;; damping needed for the function to converge:
;; nth 	minimal damp
;; 2 	1
;; 3 	1
;; 4 	2
;; 5 	2
;; 6 	2
;; 7 	2
;; 8 	3
;; 9 	3
;; 10 	3
;; 11 	3
;; 12 	3
;; 13 	3
;; 14 	3
;; 15 	3
;; 16 	4
;; 17 	4
;; … 	4
;; 30 	4
;; 31 	4
;; 32 	5
;; … 	5
;; 63 	5
;; 64 	6

;; seems like damp = log_2(nth)

(define (log2 x)
  (floor (/ (log x) (log 2))))

(define (nth-root x nth)
  (fixed-point
   ((repeated average-damp (log2 nth))
    (lambda (y)
      (/ x (pow y (- nth 1)))))
   1.0))

(nth-root 10 2)
(nth-root 1000 3)

(pow 8 10)
(nth-root 1073741824 10)

