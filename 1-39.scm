     ;; *Exercise 1.39:* A continued fraction representation of the
     ;; tangent function was published in 1770 by the German mathematician
     ;; J.H. Lambert:

     ;;                    x
     ;;      tan x = ---------------
     ;;                      x^2
     ;;              1 - -----------
     ;;                        x^2
     ;;                  3 - -------
     ;;                      5 - ...

     ;; where x is in radians.  Define a procedure `(tan-cf x k)' that
     ;; computes an approximation to the tangent function based on
     ;; Lambert's formula.  `K' specifies the number of terms to compute,
     ;; as in *Note Exercise 1-37::.


(define (cf n d k)
  (define (iter i ans)
    (if (= i 0)
        ans
        (if (= i k)
            (iter (- i 1) (/ (n i) (d i)))
            (iter (- i 1) (/ (n i) (- (d i) ans))))))
  (iter k 0))

;; Test the cf() with substraction terms
;;
;;               1
;; f() = -----------------
;;                1
;;        1 - -------------
;;                 1
;;            2 - ---------
;;                    1
;;              3 - -------
;;                    4 ...
;;
;; k = 4, f() = 2.5714

(define (test-net-cf k)
  (cf (lambda (i) 1.0)
      (lambda (i) i)
      k))

(test-net-cf 4)

(define (tan-cf x k)
  (cf (lambda (i)
        (if (= i 1)
            x
            (* x x)))
      (lambda (i)
        (+ (* (- i 1) 2) 1))
      k))

(exact->inexact (tan-cf 1 12))
(tan 1)

(exact->inexact (tan-cf 2 12))
(tan 2)

;; pi radian is 180'deg, tan(pi) is 0
(exact->inexact (tan-cf 3.14 12))
(tan 3.14)

  
