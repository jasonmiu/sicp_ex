     ;; *Exercise 1.46:* Several of the numerical methods described in
     ;; this chapter are instances of an extremely general computational
     ;; strategy known as "iterative improvement".  Iterative improvement
     ;; says that, to compute something, we start with an initial guess
     ;; for the answer, test if the guess is good enough, and otherwise
     ;; improve the guess and continue the process using the improved
     ;; guess as the new guess.  Write a procedure `iterative-improve'
     ;; that takes two procedures as arguments: a method for telling
     ;; whether a guess is good enough and a method for improving a guess.
     ;; `Iterative-improve' should return as its value a procedure that
     ;; takes a guess as argument and keeps improving the guess until it
     ;; is good enough.  Rewrite the `sqrt' procedure of section *Note
     ;; 1-1-7:: and the `fixed-point' procedure of section *Note 1-3-3::
     ;; in terms of `iterative-improve'.


;; From 1.1.7

(define (sqrt-iter guess x)
  (if (good-enough? guess x)
      guess
      (sqrt-iter (improve guess x) x)))

(define (improve guess x)
  (average guess (/ x guess)))

(define (average x y) 
  (/ (+ x y) 2))

(define (good-enough? guess x)
  (< (abs (- (square guess) x)) 0.001))

(define (sqrt x)
  (sqrt-iter 1.0 x))

(sqrt 64)


(define (iterative-improve good-enough? improve)
  (lambda (guess)
    (if (good-enough? guess) 
        guess
        ((iterative-improve good-enough? improve) (improve guess)))))

(define (sqrt-iter-imp x)
  (define (good-enough? guess)
    (< (abs (- (square guess) x)) 0.001))
  (define (improve guess)
    (average guess (/ x guess)))
  ((iterative-improve good-enough? improve) 1.0))

(sqrt-iter-imp 64)


;; From 1.3.3
;; (define tolerance 0.00001)

;; (define (fixed-point f first-guess)
;;   (define (close-enough? v1 v2)
;;     (< (abs (- v1 v2)) 
;;        tolerance))
;;   (define (try guess)
;;     (let ((next (f guess)))
;;       (if (close-enough? guess next)
;;           next
;;           (try next))))
;;   (try first-guess))



(define (fixed-point-iter-imp f first-guess)
  (define (good-enough? guess)
    (< (abs (- (f guess) guess)) 0.000001))
  (define (improve guess)
    (f guess))
  ((iterative-improve good-enough? improve) first-guess))


(fixed-point-iter-imp cos 1.0)
(fixed-point-iter-imp (lambda (y) (+ (sin y) (cos y)))
                      1.0)
    
    
  
  

  
          

